#include "Utils.h"
#include <sstream>
using namespace std;
using namespace std::tr1;

int colorPos;
int elements[2];
DWORD dwOldColors[2];
DWORD dwNewColors[2];
WCHAR lpszCapture[MAX_PATH];

CString CUtils::StrStablizer(CString str)
{
	CT2CW buf(str.MakeLower());
	wstring ws(buf);
	wstringstream wss;
	wregex pattern (_T("[a-zA-Z0-9]+"));
	wsregex_iterator iter_b(ws.begin(), ws.end(), pattern);
	wsregex_iterator iter_e;
	while (iter_b != iter_e)
	{
		wss << iter_b->str().c_str();
		iter_b++;
	}
	return CString(wss.str().c_str());
}

CString CUtils::StrNormalize(CString str)
{
	return str.Trim();
}

void CUtils::HideShellTray()
{
	RECT workWnd, taskWnd;
	HWND trayWnd = FindWindow(_T("Shell_TrayWnd"), NULL);
	HWND startBn = FindWindow(_T("Button"), _T("Start"));
	SystemParametersInfo(SPI_GETWORKAREA, 0, &workWnd, 0);
	if (trayWnd)
	{
		GetWindowRect(trayWnd, &taskWnd);
		workWnd.bottom -= (taskWnd.bottom - taskWnd.top);
		SystemParametersInfo(SPI_SETWORKAREA, 0, &workWnd, 0);
		ShowWindow(trayWnd, SW_HIDE);
	}
	if (startBn)
	{
		ShowWindow(startBn, SW_HIDE);
	}
}

void CUtils::ShowShellTray()
{
	RECT workWnd, taskWnd;
	HWND trayWnd = FindWindow(_T("Shell_TrayWnd"), NULL);
	HWND startBn = FindWindow(_T("Button"), _T("Start"));
	SystemParametersInfo(SPI_GETWORKAREA, 0, &workWnd, 0);
	if (trayWnd)
	{
		GetWindowRect(trayWnd, &taskWnd);
		workWnd.bottom += (taskWnd.bottom - taskWnd.top);
		SystemParametersInfo(SPI_SETWORKAREA, 0, &workWnd, 0);
		ShowWindow(trayWnd, SW_SHOW);
	}
	if (startBn)
	{
		ShowWindow(startBn, SW_SHOW);
	}
}

void CUtils::ChooseCaptureFolder(const WCHAR * str)
{
	wcscpy_s(lpszCapture, MAX_PATH, str);
}

wstring CUtils::Capture()
{
	using namespace Gdiplus;
	CLSID clsid;
	ULONG_PTR gdiplusToken;
	GdiplusStartupInput gdiplusStartupInput;
	HBITMAP membm;
	HDC scrdc, memdc;
	WCHAR lpszPath[MAX_PATH] = { 0 };
	wsprintfW(lpszPath, _T("%s\\%u.jpg"), lpszCapture, GetTickCount());
	GdiplusStartup(&gdiplusToken, &gdiplusStartupInput, NULL);
	scrdc = ::GetDC(0);
	int h = GetSystemMetrics(SM_CYSCREEN);
	int w = GetSystemMetrics(SM_CXSCREEN);
	memdc = CreateCompatibleDC(scrdc);
	membm = CreateCompatibleBitmap(scrdc, w, h);
	HBITMAP hOldBitmap = (HBITMAP)SelectObject(memdc, membm);
	BitBlt(memdc, 0, 0, w, h, scrdc, 0, 0, SRCCOPY);
	Gdiplus::Bitmap bitmap(membm, NULL);
	GetEncoderClsid(L"image/jpeg", &clsid);
	bitmap.Save(lpszPath, &clsid);
	SelectObject(memdc, hOldBitmap);
	DeleteObject(memdc);
	DeleteObject(membm);
	::ReleaseDC(0, scrdc);
	Gdiplus::GdiplusShutdown(gdiplusToken);
	return lpszPath;
}

void CUtils::StoreColors()
{
	elements[0] = COLOR_HIGHLIGHT;
	elements[1] = COLOR_HIGHLIGHTTEXT;
	dwNewColors[0] = RGB(240, 240, 240);
	dwNewColors[1] = RGB(0, 0, 0);
	dwOldColors[0] = GetSysColor(elements[0]);
	dwOldColors[1] = GetSysColor(elements[1]);
	colorPos = 0;
}

void CUtils::NewSysColor()
{	
	if (colorPos == 0)
	{
		SetSysColors(2, elements, dwNewColors);
		colorPos = 1;
	}
}

void CUtils::OldSysColor()
{
	if (colorPos == 1)
	{
		SetSysColors(2, elements, dwOldColors);
		colorPos = 0;
	}
}

int CUtils::GetEncoderClsid(const WCHAR* format, CLSID* pClsid)
{
	using namespace Gdiplus;
	UINT  num = 0;          // number of image encoders
	UINT  size = 0;         // size of the image encoder array in bytes
	ImageCodecInfo* pImageCodecInfo = NULL;
	GetImageEncodersSize(&num, &size);
	if (size == 0)
		return -1;  // Failure
	pImageCodecInfo = (ImageCodecInfo*)(malloc(size));
	if (pImageCodecInfo == NULL)
		return -1;  // Failure
	GetImageEncoders(num, size, pImageCodecInfo);
	for (UINT j = 0; j < num; ++j)
	{
		if (wcscmp(pImageCodecInfo[j].MimeType, format) == 0)
		{
			*pClsid = pImageCodecInfo[j].Clsid;
			free(pImageCodecInfo);
			return j;  // Success
		}
	}
	free(pImageCodecInfo);
	return 0;
}
