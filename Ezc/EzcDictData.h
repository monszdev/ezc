#pragma once
#include "defs.h"
#include "Utils.h"
using namespace std;

class CEzcDictData
{
public:
	CString m_strAnsw;
	CString m_strQues;
	CString m_strMini;
public:
	CEzcDictData(CString line)
	{
		int spos = line.Find(_T("|"));
		CString ques = line.Mid(0,spos);
		CString answ = line.Mid(spos+1);
		import(answ, ques);
	}
	CEzcDictData(CString answ, CString ques)
	{
		import(answ, ques);
	}
	CEzcDictData(const CEzcDictData& ezcDictData)
	{
		this->m_strAnsw = ezcDictData.m_strAnsw;
		this->m_strQues = ezcDictData.m_strQues;
		this->m_strMini = ezcDictData.m_strMini;
	}
	bool Check(CString str)
	{
		return (m_strMini.Find(str) != -1);
	}
private:
	void import(CString answ, CString ques)
	{
		m_strAnsw = CUtils::StrNormalize(answ);
		m_strQues = CUtils::StrNormalize(ques);
		m_strMini = CUtils::StrStablizer(m_strQues+m_strAnsw);
	}
};

