#pragma once
#include "defs.h"
#include "EzcDictData.h"
using namespace std;

class CEzcDict
{
public:
	vector<CEzcDictData> m_lstData;
public:
	void import(CString strFilePath);
	void search(CString strQuery, vector<CEzcDictData>* pResult);
};

