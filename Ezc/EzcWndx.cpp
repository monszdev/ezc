
#include "Utils.h"
#include "EzcWndx.h"
#include "EzcDict.h"
#include "Mapping.h"
#include "resource.h"

HWND hEzcDictWnd;
CEzcDict mEzcDict;
CMapping mMapping;
bool bIsEnterSearching = false;

void EzcCreateWindow()
{
	CreateThread(0, 0, (LPTHREAD_START_ROUTINE)EzcThreadWindow, 0, 0, 0);
	CreateThread(0, 0, (LPTHREAD_START_ROUTINE)EzcThreadLookup, 0, 0, 0);
}

void EzcThreadWindow()
{
	DialogBox(GetModuleHandle(_T("Ezc.dll")), MAKEINTRESOURCE(IDD_DLGDICT), 0, (DLGPROC)EzcWindowProc);
}

void EzcThreadLookup()
{
	do {
		EzcCenterWindow();
		if (bIsEnterSearching)
		{
			CUtils::NewSysColor();
			EzcSearchAnswer();
			EzcChangeOpaque(200);
		}
		else
		{
			EzcChangeOpaque(0);
			EzcSetAnswLabel(_T(""));
			EzcSetQuesLabel(_T(""));
			CUtils::OldSysColor();
		}
		Sleep(10);
	} while (true);
}

void EzcCenterWindow()
{
	RECT rect;
	HWND hwnd = EzcGetWndHandle();
	if (hwnd != NULL)
	{
		GetWindowRect(hwnd, &rect);
		int w = rect.right - rect.left;
		int h = rect.bottom - rect.top;
		int nScreenW = GetSystemMetrics(SM_CXSCREEN);
		int nScreenH = GetSystemMetrics(SM_CYSCREEN);
		int x = (nScreenW - w) / 2;
		int y = (nScreenH - h);
		SetWindowPos(hwnd, HWND_TOPMOST, x, y, w, h, SWP_NOSIZE);
	}
}

void EzcSearchAnswer()
{
	vector<CEzcDictData> result;
	CString query = EzcGetSelection();
	if (query == _T("")) return;
	mEzcDict.search(query, &result);
	if (result.size() > 0)
	{
		EzcSetAnswLabel(result.at(0).m_strAnsw);
		EzcSetQuesLabel(result.at(0).m_strQues);
	}
	else
	{
		EzcSetQuesLabel(query);
		EzcSetAnswLabel(_T(""));
	}
}

BYTE EzcBackupOpaque()
{
	HWND hwnd = EzcGetWndHandle();
	if (hwnd != NULL)
	{
		BYTE alpha;
		DWORD colorRef, flags;
		UNREFERENCED_PARAMETER(flags);
		UNREFERENCED_PARAMETER(colorRef);
		GetLayeredWindowAttributes(hwnd, (COLORREF*)&colorRef, &alpha, (DWORD*)&flags);
		return alpha;
	}
	return 0;
}

void EzcChangeOpaque(BYTE alpha)
{
	HWND hwnd = EzcGetWndHandle();
	if (hwnd != NULL)
	{
		SetLayeredWindowAttributes(hwnd, RGB(240, 240, 240), alpha, LWA_ALPHA | LWA_COLORKEY);
	}
}

void EzcSetQuesLabel(CString str)
{
	SetDlgItemText(hEzcDictWnd, IDC_QUES, str);
}

void EzcSetAnswLabel(CString str)
{
	SetDlgItemText(hEzcDictWnd, IDC_ANSW, str);
}

void EzcShow()
{
	ShowWindow(EzcGetWndHandle(), SW_SHOWNOACTIVATE);
}

void EzcHide()
{
	ShowWindow(EzcGetWndHandle(), SW_HIDE);
}

void EzcEnterSearchMode()
{
	bIsEnterSearching = true;
}

void EzcLeaveSearchMode()
{
	bIsEnterSearching = false;
	SendMessage(EzcGetWndCursor(), EM_SETSEL, 0, 0);
}

HWND EzcGetWndCursor()
{
	POINT pt;
	GetCursorPos(&pt);
	HWND wnd = WindowFromPoint(pt);
	return wnd;
}

HWND EzcGetWndHandle()
{
	return hEzcDictWnd;
}

LONG EzcCreateStyles(HWND hwnd)
{
	LONG styles = GetWindowLong(hwnd, GWL_EXSTYLE);
	return (styles | WS_EX_LAYERED | WS_EX_TOPMOST | WS_EX_TOOLWINDOW & ~(WS_EX_APPWINDOW));
}

CString EzcGetSelection()
{
	HWND selectWnd = EzcGetWndCursor();
	if (selectWnd != NULL)
	{
		int n, m;
		int size = GetWindowTextLength(selectWnd);
		LPTSTR buf = new TCHAR [size];
		GetWindowText(selectWnd, buf, size);
		SendMessage(selectWnd, EM_GETSEL, (WPARAM)&n, (LPARAM)&m);
		CString strBuf(CString(buf).Mid(n, m - n));
		delete buf;
		return strBuf;
	}
	return _T("");
}

BOOL WINAPI EzcWindowProc(HWND hwnd, UINT uMsg, WPARAM wParam, LPARAM lParam)
{
	switch (uMsg)
	{
	case WM_INITDIALOG:
		MapInfo info;
		if (!mMapping.UpdateConfigs(&info))
		{
			ExitProcess(0);
		}
		hEzcDictWnd = hwnd;
		mEzcDict.import(info.lpszDict);
		CUtils::ChooseCaptureFolder(info.lpszFold);
		SendMessage(info.mainHwnd, WM_CLOSE, 0, 0);
		SetWindowLong(hwnd, GWL_EXSTYLE, EzcCreateStyles(hwnd));
		return TRUE;
	}
	return FALSE;
}
