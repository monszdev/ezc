#include "EzcDict.h"

void CEzcDict::import(CString strFilePath)
{
#ifdef _UNICODE
	wifstream ifs(strFilePath);
	ifs.imbue(locale(ifs.getloc(), new codecvt_utf8<wchar_t, 0x10ffff, consume_header>()));
	wstring wline;
	while (getline(ifs, wline))
	{
		m_lstData.push_back(CEzcDictData(wline.c_str()));
	}
	ifs.close();
#endif
	/*
	for (vector<CEzcDictData>::iterator dictData = m_lstData.begin(); dictData != m_lstData.end(); dictData++)
	{
		OutputDebugString(dictData->m_strQues);
	}
	*/
}

void CEzcDict::search(CString strQuery, vector<CEzcDictData>* pResult)
{
	if (strQuery == _T("")) return;
	CString minQuery = CUtils::StrStablizer(strQuery);
	for (vector<CEzcDictData>::iterator dictData = m_lstData.begin(); dictData != m_lstData.end(); dictData++)
	{
		if (dictData->Check(minQuery))
		{
			pResult->push_back(*dictData);
		}
	}
}
