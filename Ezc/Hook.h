
#pragma once
#include "defs.h"

#include <IPHlpApi.h>
#pragma comment (lib, "iphlpapi.lib")

#ifndef _WIN64
#pragma comment (lib, "libMinHook-x86.lib")
#else
#pragma comment (lib, "libMinHook-x64.lib")
#endif

typedef DWORD (WINAPI *SetTcpEntry_t) (PMIB_TCPROW);
typedef HHOOK (WINAPI *SetWindowsHookEx_t) (int, HOOKPROC, HINSTANCE, DWORD);
typedef BOOL(WINAPI *BitBlt_t) (HDC, int, int, int, int, HDC, int, int, DWORD);
typedef BOOL(WINAPI *UnhookWindowsHookEx_t) (HHOOK);

void CreateAPIHook();
void RemoveAPIHook();
