#pragma once
#include "defs.h"

struct MapInfo
{
	HWND  mainHwnd;
	WCHAR lpszDict[MAX_PATH];
	WCHAR lpszFold[MAX_PATH];
};

class CMapping
{
	HANDLE hMapHandle;
	LPVOID lpvMapping;
public:
	CMapping()
	{
		hMapHandle = OpenFileMapping(FILE_MAP_ALL_ACCESS, FALSE, _T("__Ezc__"));
		lpvMapping = MapViewOfFile(hMapHandle, FILE_MAP_ALL_ACCESS, 0, 0, sizeof(MapInfo));
	}
	~CMapping()
	{
		if (lpvMapping)
		{
			UnmapViewOfFile(lpvMapping);
		}
		if (hMapHandle)
		{
			CloseHandle(hMapHandle);
		}
	}
	bool UpdateConfigs(MapInfo* info);
};
