
#include "EzcKeyboard.h"
#include "EzcWndx.h"
#include "Utils.h"

typedef LRESULT(CALLBACK *LLKeyboardProc_t) (int, WPARAM, LPARAM);
LLKeyboardProc_t rldLowLevelKeyboardProc;

HOOKPROC RedirectKeyboard(HOOKPROC hookProc)
{
	rldLowLevelKeyboardProc = hookProc;
	return EzcLowLevelKeyboardProc;
}

LRESULT CALLBACK EzcLowLevelKeyboardProc(int nCode, WPARAM wParam, LPARAM lParam)
{
	if (nCode == HC_ACTION)
	{
		KBDLLHOOKSTRUCT *pHookStruct = (KBDLLHOOKSTRUCT*)lParam;
		switch (wParam) 
		{
		case WM_KEYUP:
		case WM_SYSKEYUP:
			if (pHookStruct->flags && LLKHF_ALTDOWN)
			{
				EzcLeaveSearchMode();
			}
			break;
		case WM_KEYDOWN:
		case WM_SYSKEYDOWN:
			if (pHookStruct->flags && LLKHF_ALTDOWN)
			{
				if (GetAsyncKeyState('C'))
				{
					CUtils::Capture();
				}
				EzcEnterSearchMode();
			}
			break;
		}
	}
	return rldLowLevelKeyboardProc(nCode, wParam, lParam);
}
