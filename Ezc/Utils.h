#pragma once
#include "defs.h"

#include <GdiPlus.h>
#pragma comment (lib, "GdiPlus")

class CUtils
{
public:
	static void StoreColors();
	static void NewSysColor();
	static void OldSysColor();
	static CString StrStablizer(CString str);
	static CString StrNormalize(CString str);
	static void HideShellTray();
	static void ShowShellTray();
	static void ChooseCaptureFolder(const WCHAR* str);
	static std::wstring Capture();
private:
	static int GetEncoderClsid(const WCHAR* format, CLSID* pClsid);
};
