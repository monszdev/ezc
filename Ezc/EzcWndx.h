
#pragma once
#include "defs.h"

void EzcCreateWindow();
void EzcThreadWindow();
void EzcThreadLookup();
void EzcCenterWindow();
void EzcSearchAnswer();
BYTE EzcBackupOpaque();
void EzcChangeOpaque(BYTE alpha);
void EzcSetQuesLabel(CString str);
void EzcSetAnswLabel(CString str);
void EzcShow();
void EzcHide();
void EzcEnterSearchMode();
void EzcLeaveSearchMode();
HWND EzcGetWndCursor();
HWND EzcGetWndHandle();
LONG EzcCreateStyles(HWND hwnd);
CString EzcGetSelection();

BOOL WINAPI EzcWindowProc(HWND hwnd, UINT uMsg, WPARAM wParam, LPARAM lParam);
