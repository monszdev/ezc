
#include "defs.h"
#include "Hook.h"
#include "EzcWndx.h"
#include "Utils.h"

BOOL APIENTRY DllMain(HMODULE hModule, DWORD dwReason, LPVOID lpReserved)
{
	switch (dwReason)
	{
	case DLL_PROCESS_ATTACH:
		CUtils::StoreColors();
		CreateMutex(NULL, TRUE, _T("__EzcMutex__"));
		if (GetLastError() == ERROR_ALREADY_EXISTS)
		{
			CreateAPIHook();
			EzcCreateWindow();
		}
		break;
	case DLL_PROCESS_DETACH:
		RemoveAPIHook();
		CUtils::ShowShellTray();
		break;
	}
	return TRUE;
}
