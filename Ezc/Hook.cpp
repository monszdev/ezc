
#include "Hook.h"
#include "EzcMouse.h"
#include "EzcKeyboard.h"
#include "EzcWndx.h"
#include "Utils.h"

BitBlt_t rldBitBlt;
SetTcpEntry_t rldSetTcpEntry;
SetWindowsHookEx_t rldSetWindowsHookEx;
UnhookWindowsHookEx_t rldUnhookWindowsHookEx;
bool  bIsHooked = false;
HHOOK hKeyboardHook;

BOOL WINAPI fckBitBlt (HDC dHdc, int dx, int dy, int dw, int dh, HDC sHdc, int sx, int sy, DWORD rop)
{
	BYTE opaque = EzcBackupOpaque();
	EzcChangeOpaque(0);
	BOOL result = rldBitBlt(dHdc, dx, dy, dw, dh, sHdc, sx, sy, rop);
	EzcChangeOpaque(opaque);
	return result;
}

DWORD WINAPI fckSetTcpEntry(PMIB_TCPROW r)
{
	return 0;
}

HHOOK WINAPI fckSetWindowsHookEx(int hkId, HOOKPROC hookProc, HINSTANCE hInst, DWORD threadId)
{
	if (hkId == WH_MOUSE_LL) {
		hookProc = RedirectMouse(hookProc);
	} else if (hkId == WH_KEYBOARD_LL) {
		hookProc = RedirectKeyboard(hookProc);
	}
	HHOOK hhk = rldSetWindowsHookEx(hkId, hookProc, hInst, threadId);
	if (hkId == WH_KEYBOARD_LL) {
		hKeyboardHook = hhk;
		CUtils::HideShellTray();
	}
	return hhk;
}

BOOL WINAPI fckUnhookWindowsHookEx(HHOOK hhk)
{
	if (hhk == hKeyboardHook) {
		CUtils::ShowShellTray();
	}
	return rldUnhookWindowsHookEx(hhk);
}

void CreateAPIHook()
{
	if (MH_Initialize() == MH_OK) {
		MH_CreateHook(&BitBlt, &fckBitBlt, (LPVOID*)&rldBitBlt);
		MH_CreateHook(&SetTcpEntry, &fckSetTcpEntry, (LPVOID*)&rldSetTcpEntry);
		MH_CreateHook(&SetWindowsHookExW, &fckSetWindowsHookEx, (LPVOID*)&rldSetWindowsHookEx);
		MH_CreateHook(&UnhookWindowsHookEx, &fckUnhookWindowsHookEx, (LPVOID*)&rldUnhookWindowsHookEx);
		MH_EnableHook(&BitBlt);
		MH_EnableHook(&SetTcpEntry);
		MH_EnableHook(&SetWindowsHookExW);
		MH_EnableHook(&UnhookWindowsHookEx);
		bIsHooked = true;
	}
}

void RemoveAPIHook()
{
	if (bIsHooked)
	{
		MH_DisableHook(&BitBlt);
		MH_DisableHook(&SetTcpEntry);
		MH_DisableHook(&SetWindowsHookExW);
		MH_DisableHook(&UnhookWindowsHookEx);
		MH_Uninitialize();
	}
}
