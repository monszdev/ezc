#include "EzcMouse.h"
#include "EzcWndx.h"

typedef LRESULT(CALLBACK *LLMouseProc_t) (int, WPARAM, LPARAM);
LLMouseProc_t rldLowLevelMouseProc;

void SendMouseEvent(bool isDown)
{
	INPUT input;
	input.type = INPUT_MOUSE;
	input.mi.dx = 0;
	input.mi.dy = 0;
	input.mi.mouseData = 0;
	input.mi.dwExtraInfo = 0;
	input.mi.time = 0;
	input.mi.dwFlags = MOUSEEVENTF_ABSOLUTE | (isDown ? MOUSEEVENTF_LEFTDOWN : MOUSEEVENTF_LEFTUP);
	SendInput(1, &input, sizeof(INPUT));
}

HOOKPROC RedirectMouse(HOOKPROC hookProc)
{
	rldLowLevelMouseProc = hookProc;
	return EzcLowLevelMouseProc;
}

LRESULT EzcLowLevelMouseProc(int nCode, WPARAM wParam, LPARAM lParam)
{
	switch (wParam)
	{
	case 523:
		EzcEnterSearchMode();
		SendMouseEvent(true);
		return rldLowLevelMouseProc(nCode, 513, lParam);
		break;
	case 524:
		EzcLeaveSearchMode();
		SendMouseEvent(false);
		return rldLowLevelMouseProc(nCode, 514, lParam);
		break;
	}
	return rldLowLevelMouseProc(nCode, wParam, lParam);
}
