#include "Process.h"

DWORD CProcess::GetProcessId(LPCTSTR lpProcessName)
{
	DWORD pid;
	PROCESSENTRY32 lppe;
	ZeroMemory(&lppe, sizeof(PROCESSENTRY32));
	lppe.dwSize = sizeof(PROCESSENTRY32);
	HANDLE hSnapshot = CreateToolhelp32Snapshot(TH32CS_SNAPPROCESS, 0);
	Process32First(hSnapshot, &lppe);
	do {
		if (wcsicmp(lppe.szExeFile, lpProcessName) == 0)
		{
			pid = lppe.th32ProcessID;
			break;
		}
	} while (Process32Next(hSnapshot, &lppe));
	CloseHandle(hSnapshot);
	return pid;
}

void CProcess::InjectModule(DWORD dwProcessId, LPCSTR lpszModule)
{
	HANDLE hProcess = OpenProcess(PROCESS_ALL_ACCESS, FALSE, dwProcessId);
	LPVOID __LoadLibrary__ = (LPVOID)GetProcAddress(GetModuleHandle(_T("kernel32")), ("LoadLibraryA"));
	LPVOID __PathLibrary__ = (LPVOID)VirtualAllocEx(hProcess, NULL, strlen(lpszModule), MEM_RESERVE | MEM_COMMIT, PAGE_READWRITE);
	WriteProcessMemory(hProcess, __PathLibrary__, lpszModule, strlen(lpszModule), NULL);
	HANDLE hThread = CreateRemoteThread(hProcess, NULL, NULL, (LPTHREAD_START_ROUTINE)__LoadLibrary__, __PathLibrary__, 0, 0);
	WaitForSingleObject(hThread, INFINITE);
	VirtualFreeEx(hProcess, __PathLibrary__, strlen(lpszModule), MEM_RELEASE);
	CloseHandle(hThread);
	CloseHandle(hProcess);
}

bool CProcess::InjectModule(LPCTSTR lpProcessName, LPCSTR lpszModule)
{
	bool result = false;
	PROCESSENTRY32 lppe;
	ZeroMemory(&lppe, sizeof(PROCESSENTRY32));
	lppe.dwSize = sizeof(PROCESSENTRY32);
	HANDLE hSnapshot = CreateToolhelp32Snapshot(TH32CS_SNAPPROCESS, 0);
	Process32First(hSnapshot, &lppe);
	do {
#ifdef _UNICODE
		if (wcsicmp(lppe.szExeFile, lpProcessName) == 0)
#else
		if (stricmp(lppe.szExeFile, lpProcessName) == 0)
#endif
		{
			InjectModule(lppe.th32ProcessID, lpszModule);
			result = true;
		}
	} while (Process32Next(hSnapshot, &lppe));
	CloseHandle(hSnapshot);
	return result;
}
