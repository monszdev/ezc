#include "License.h"
#pragma warning (disable : 4244 4267)

bool CLicense::ValidateLicense()
{
	FILE *fk, *fl;
	errno_t rfk = fopen_s(&fk, "Ezc.key", "rb");
	errno_t rfl = fopen_s(&fl, "Ezc.lic", "rb");
	if (rfk == 0)
	{
		fseek(fk, 0, 2);
		size_t sz = ftell(fk);
		fseek(fk, 0, 0);
		BYTE* hwa = new BYTE[6];
		BYTE* key = new BYTE[sz];
		BYTE* lic = new BYTE[sz];
		fread(key, sz, 1, fk);
		fclose(fk);
		if (rfl == 0)
		{
			fread(lic, sz, 1, fl);
			fclose(fl);
			BYTE rnd = key[0];
			hwa[0] = key[1 * rnd];
			hwa[1] = key[2 * rnd];
			hwa[2] = key[3 * rnd];
			hwa[3] = key[4 * rnd];
			hwa[4] = key[5 * rnd];
			hwa[5] = key[6 * rnd];
			BYTE* currentHardware = GetEthernetHardwareAddress();
			// validate key
			if (currentHardware[0] == hwa[0] &&
				currentHardware[1] == hwa[1] &&
				currentHardware[2] == hwa[2] &&
				currentHardware[3] == hwa[3] &&
				currentHardware[4] == hwa[4] &&
				currentHardware[5] == hwa[5])
			{
				// validate lic
				bool result = true;
				for (size_t i = 0; i < sz; i++)
				{
					if (lic[i] != (key[i] | rnd))
					{
						result = false;
					}
				}
				return result;
			}
		}
	}
	return false;
}

void CLicense::GenerateKeyPair()
{
	srand(time(0));
	BYTE rnd = rand() % 0xfe + 1;
	BYTE* temporaryString = new BYTE[rnd * 6 + 2];
	BYTE* hardwareAddress = GetEthernetHardwareAddress();
	for (int i = 0; i < rnd * 6 + 2; i++)
	{
		temporaryString[i] = (rand() % 0xff);
	}
	temporaryString[0] = rnd;
	temporaryString[1 * rnd] = hardwareAddress[0];
	temporaryString[2 * rnd] = hardwareAddress[1];
	temporaryString[3 * rnd] = hardwareAddress[2];
	temporaryString[4 * rnd] = hardwareAddress[3];
	temporaryString[5 * rnd] = hardwareAddress[4];
	temporaryString[6 * rnd] = hardwareAddress[5];
	FILE* fw = fopen("Ezc.key", "wb");
	fwrite(temporaryString, rnd * 6 + 2, 1, fw);
	fclose(fw);
}

BYTE* CLicense::GetEthernetHardwareAddress()
{
	PIP_ADAPTER_INFO AdapterInfo;
	DWORD dwBufLen = sizeof(AdapterInfo);
	CHAR *lpszMacAddress = new CHAR[MAC_LENGTH];
	AdapterInfo = (IP_ADAPTER_INFO *)malloc(sizeof(IP_ADAPTER_INFO));
	if (AdapterInfo == NULL) {
		return NULL;
	}
	if (GetAdaptersInfo(AdapterInfo, &dwBufLen) == ERROR_BUFFER_OVERFLOW) {
		AdapterInfo = (IP_ADAPTER_INFO *)malloc(dwBufLen);
		if (AdapterInfo == NULL) {
			return NULL;
		}
	}
	if (GetAdaptersInfo(AdapterInfo, &dwBufLen) == NO_ERROR) {
		PIP_ADAPTER_INFO pAdapterInfo = AdapterInfo;
		do {
			if (pAdapterInfo->Type == MIB_IF_TYPE_ETHERNET) {
				/*
				sprintf_s(lpszMacAddress,
					MAC_LENGTH,
					("%02X%02X%02X%02X%02X%02X"),
					pAdapterInfo->Address[0], pAdapterInfo->Address[1],
					pAdapterInfo->Address[2], pAdapterInfo->Address[3],
					pAdapterInfo->Address[4], pAdapterInfo->Address[5]);
					*/
				return pAdapterInfo->Address;
			}
			pAdapterInfo = pAdapterInfo->Next;
		} while (pAdapterInfo);
	}
	free(AdapterInfo);
	return NULL;
}
