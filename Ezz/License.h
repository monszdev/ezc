#pragma once
#include "defs.h"
#include <time.h>
#include <IPHlpApi.h>
#pragma comment (lib, "iphlpapi.lib")

#define MAC_LENGTH 20

struct KeyInfo
{
	int rnd;
	CHAR lpszHardwareAddress[MAC_LENGTH];
};

class CLicense
{
public:
	bool ValidateLicense();
	void GenerateKeyPair();
	static BYTE* GetEthernetHardwareAddress();
};

