#pragma once
#include "defs.h"
using namespace std;

struct MapInfo
{
	HWND  mainHwnd;
	WCHAR lpszDict[MAX_PATH];
	WCHAR lpszFold[MAX_PATH];
};

class CMapping
{
	HANDLE hMapHandle;
	LPVOID lpvMapping;
public:
	CMapping()
	{
		hMapHandle = CreateFileMapping(INVALID_HANDLE_VALUE, NULL, PAGE_READWRITE, 0, sizeof(MapInfo), _T("__Ezc__"));
		lpvMapping = MapViewOfFile(hMapHandle, FILE_MAP_ALL_ACCESS, 0, 0, sizeof(MapInfo));
	}
	~CMapping()
	{
		if (lpvMapping)
		{
			UnmapViewOfFile(lpvMapping);
		}
		if (hMapHandle)
		{
			CloseHandle(hMapHandle);
		}
	}
	void UpdateConfigs(wstring dict, wstring fold, HWND hwnd);
	void UpdateConfigs(MapInfo info);
};

