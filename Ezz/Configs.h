#pragma once
#include "defs.h"

using namespace std;
class CConfigs
{
private:
	CIniFile *ini;
public:
	CConfigs() : ini (NULL)
	{
		ini = new CIniFile();
		ini->Load(_T("Ezc.ini"));
	}
	~CConfigs()
	{
		if (ini != NULL)
		{
			delete ini;
		}
	}
	wstring GetDictionary();
	wstring GetCaptureFolder();
	void SetDictionary(wstring path);
	void SetCaptureFolder(wstring path);
	void Save();
};

