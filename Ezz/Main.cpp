
#include "defs.h"
#include "resource.h"
#include "Configs.h"
#include "Process.h"
#include "Mapping.h"
#include "License.h"

#pragma comment (linker,"\"/manifestdependency:type='win32' \
						name='Microsoft.Windows.Common-Controls' version='6.0.0.0' \
						processorArchitecture='*' publicKeyToken='6595b64144ccf1df' language='*'\"")

CConfigs *configs;
CMapping *mapping;
CLicense *license;
CString strCommandLine;
void LoadCommand(HWND hwnd);
void LoadConfigs(HWND hwnd);
wstring ChooseDictionary(HWND hwnd);
wstring ChooseCaptureFolder(HWND hwnd);
int CALLBACK BrowseFolderCallbackProc(HWND hwnd, UINT msg, LPARAM l, LPARAM data);
void ThreadInjection();

LRESULT WINAPI DialogProc(HWND hwnd, UINT msg, WPARAM w, LPARAM l)
{
	switch (msg)
	{
	case WM_CLOSE:
		return DefWindowProc(hwnd, msg, w, l);
	case WM_INITDIALOG:
		license = new CLicense();
		if (license->ValidateLicense() == false)
		{
			license->GenerateKeyPair();
			ShowWindow(GetDlgItem(hwnd, IDC_STA_INFO), 5);
			ShowWindow(GetDlgItem(hwnd, IDC_BTN_DICT), 0);
			ShowWindow(GetDlgItem(hwnd, IDC_BTN_CAPT), 0);
			ExitProcess(0);
			return TRUE;
		}
		configs = new CConfigs();
		mapping = new CMapping();
		LoadConfigs(hwnd);
		SendMessage(hwnd, WM_SYSCOMMAND, SC_MINIMIZE, 0);
		CreateThread(0, 0, (LPTHREAD_START_ROUTINE)ThreadInjection, 0, 0, 0);
		LoadCommand(hwnd);
		return TRUE;
	case WM_COMMAND:
		switch (LOWORD(w))
		{
		case IDC_BTN_DICT:
			configs->SetDictionary(ChooseDictionary(hwnd));
			configs->Save();
			LoadConfigs(hwnd);
			break;
		case IDC_BTN_CAPT:
			configs->SetCaptureFolder(ChooseCaptureFolder(hwnd));
			configs->Save();
			LoadConfigs(hwnd);
			break;
		}
		break;
	}
	return FALSE;
}

int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR lpCommandLine, int nShowCmd)
{
	HANDLE mtx = CreateMutex(NULL, TRUE, _T("__EzcMutex__"));
	if (GetLastError() == ERROR_ALREADY_EXISTS)
	{
		ReleaseMutex(mtx);
		return 0;
	}
	strCommandLine = CString(lpCommandLine);
	DialogBox(hInstance, MAKEINTRESOURCE(IDD_MAIN), 0, DialogProc);
	ReleaseMutex(mtx);
	return 0;
}

void LoadCommand(HWND hwnd)
{
	if (strCommandLine.IsEmpty() == false)
	{
		int argc;
		LPTSTR *args = CommandLineToArgvW(strCommandLine, &argc);
		CString strEosPath, strDicPath, strCapPath;
		for (int i = 0; i < argc; i++)
		{
			CString dir;
			CString opt = CString(args[i]);
			if ((i + 1) < argc) { dir = CString(args[i + 1]); }
			if (dir.IsEmpty() == false)
			{
				if (opt.CompareNoCase(_T("-e")) == 0)
				{
					strEosPath = dir;
				}
				else if (opt.CompareNoCase(_T("-d")) == 0)
				{
					strDicPath = dir;
				}
				else if (opt.CompareNoCase(_T("-c")) == 0)
				{
					strCapPath = dir;
				}
			}
		}
		if (strDicPath.IsEmpty() == false) 
		{
			configs->SetDictionary(wstring(strDicPath));
			SetDlgItemText(hwnd, IDC_EDI_DICT, strDicPath);
			LoadConfigs(hwnd);
		}
		if (strCapPath.IsEmpty() == false)
		{
			configs->SetCaptureFolder(wstring(strCapPath));
			SetDlgItemText(hwnd, IDC_EDI_CAPT, strCapPath);
			LoadConfigs(hwnd);
		}
		if (strEosPath.IsEmpty() == false)
		{
			CString strWorkingDir = CString(strEosPath);
			LPTSTR lpszWorkingDir = strWorkingDir.GetBuffer(0);
			strWorkingDir.ReleaseBuffer();
			PathRemoveFileSpec(lpszWorkingDir);
			ShellExecute(0, _T("runas"), strEosPath, 0, lpszWorkingDir, SW_SHOWDEFAULT);
		}
	}
}

void LoadConfigs(HWND hwnd)
{
	wstring dict = configs->GetDictionary();
	wstring fold = configs->GetCaptureFolder();
	SetDlgItemText(hwnd, IDC_EDI_DICT, dict.c_str());
	SetDlgItemText(hwnd, IDC_EDI_CAPT, fold.c_str());
	mapping->UpdateConfigs(dict, fold, hwnd);
}

wstring ChooseDictionary(HWND hwnd)
{
	OPENFILENAME ofn;
	WCHAR lpszWorkDirs[MAX_PATH] = { 0 };
	WCHAR lpszFileName[MAX_PATH] = { 0 };
	GetCurrentDirectory(MAX_PATH, lpszWorkDirs);
	ZeroMemory(&ofn, sizeof(OPENFILENAME));
	ofn.lStructSize = sizeof(OPENFILENAME);
	ofn.hwndOwner = hwnd;
	ofn.lpstrFile = lpszFileName;
	ofn.lpstrFilter = _T("All Files (*.*)\0*.*\0");
	ofn.nMaxFile = MAX_PATH;
	ofn.Flags = OFN_EXPLORER | OFN_FILEMUSTEXIST | OFN_HIDEREADONLY;
	if (GetOpenFileName(&ofn))
	{
		SetCurrentDirectory(lpszWorkDirs);
		return lpszFileName;
	}
	return _T("");
}

wstring ChooseCaptureFolder(HWND hwnd)
{
	BROWSEINFO bi;
	WCHAR lpszFolderPath[MAX_PATH] = { 0 };
	ZeroMemory(&bi, sizeof(BROWSEINFO));
	bi.ulFlags = BIF_RETURNONLYFSDIRS | BIF_NEWDIALOGSTYLE;
	bi.lpfn = BrowseFolderCallbackProc;
	bi.hwndOwner = hwnd;
	LPITEMIDLIST idl = SHBrowseForFolder(&bi);
	if (idl != 0)
	{
		SHGetPathFromIDList(idl, lpszFolderPath);
		IMalloc * imalloc = 0;
		if (SUCCEEDED(SHGetMalloc(&imalloc)))
		{
			imalloc->Free(idl);
			imalloc->Release();
		}
		return lpszFolderPath;
	}
	return _T("");
}

int CALLBACK BrowseFolderCallbackProc(HWND hwnd, UINT msg, LPARAM l, LPARAM data)
{
	if (msg == BFFM_INITIALIZED)
	{
		SendMessage(hwnd, BFFM_SETSELECTION, TRUE, data);
	}
	return 0;
}

void ThreadInjection()
{
	CHAR lpszDll[MAX_PATH] = { 0 };
	GetCurrentDirectoryA(MAX_PATH, lpszDll);
	strcat_s(lpszDll, MAX_PATH, ("\\Ezc.dll"));
	do {
		CProcess::InjectModule(_T("EOSClient.exe"), lpszDll);
		Sleep(10);
	} while (true);
}

