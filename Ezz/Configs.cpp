#include "Configs.h"

wstring CConfigs::GetDictionary()
{
	return ini->GetKeyValue(_T("Ezc"), _T("Dictionary"));
}

wstring CConfigs::GetCaptureFolder()
{
	return ini->GetKeyValue(_T("Ezc"), _T("CaptureFolder"));
}

void CConfigs::SetDictionary(wstring path)
{
	if (!path.empty()) 
	{
		ini->SetKeyValue(_T("Ezc"), _T("Dictionary"), path);
	}
}

void CConfigs::SetCaptureFolder(wstring path)
{
	if (!path.empty())
	{
		ini->SetKeyValue(_T("Ezc"), _T("CaptureFolder"), path);
	}
}

void CConfigs::Save()
{
	WCHAR dir[MAX_PATH] = { 0 };
	GetCurrentDirectory(MAX_PATH, dir);
	wcscat_s(dir, MAX_PATH, _T("\\Ezc.ini"));
	ini->Save(dir);
}
