#pragma once
#include "defs.h"
#include <TlHelp32.h>

class CProcess
{
public:
	static DWORD GetProcessId(LPCTSTR lpProcessName);
	static void  InjectModule(DWORD dwProcessId, LPCSTR lpszModule);
	static bool  InjectModule(LPCTSTR lpProcessName, LPCSTR lpszModule);
};

