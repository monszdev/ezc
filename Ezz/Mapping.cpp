#include "Mapping.h"

void CMapping::UpdateConfigs(wstring dict, wstring fold, HWND hwnd)
{
	MapInfo info;
	info.mainHwnd = hwnd;
	wcscpy_s(info.lpszDict, MAX_PATH, dict.c_str());
	wcscpy_s(info.lpszFold, MAX_PATH, fold.c_str());
	UpdateConfigs(info);
}

void CMapping::UpdateConfigs(MapInfo info)
{
	CopyMemory(lpvMapping, &info, sizeof(MapInfo));
}
